
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from flask_restful import Api, Resource, reqparse
import logging



###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient('dockerrestapi_db_1', 27017)
db = client.tododb

#---------------------------------------------------------------------------------------------------
data_saved = ""



@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404



@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    #---------------------------------------------------------------------------------------------------
    km = request.args.get('km', 999, type=float)
    beg_date = request.args.get("beg_date",type = str)
    beg_time = request.args.get("beg_time",type = str)
    distance = request.args.get("distance",type = int)
    location = request.args.get("location",type=str)
    # ---------------------------------------------------------------------------------------------------
    date_time = arrow.utcnow()
    date = arrow.get(beg_date+" "+beg_time,'YYYY-MM-DD HH:mm').isoformat()
    #---------------------------------------------------------------------------------------------------
    date_time = arrow.get(date)
    #---------------------------------------------------------------------------------------------------
    date_time=date_time.replace(tzinfo='US/Pacific')
    date_time = date_time.isoformat()


    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, distance, date_time)
    close_time = acp_times.close_time(km, distance, date_time)
    result = {"open": open_time, "close": close_time}


    #---------------------------------------------------------------------------------------------------
    global data_saved

    data_saved = data_saved + 'Location: '+ location + '<br/>'\
        +'     Brevet_Distance: ' + str(distance) + '<br/>'\
        + '     KM: ' + str(km) + '<br/>'\
        + '     Open: ' + open_time + '<br/>'\
        + '     Close: '  + close_time + " " +'<br/>' + '<br/>'
    return flask.jsonify(result=result)

#---------------------------------------------------------------------------------------------------
@app.route('/_submit', methods=['POST'])
def new():
    global data_saved

    #---------------------------------------------------------------------------------------------------
    data = {'data':data_saved}
    #---------------------------------------------------------------------------------------------------
    if(len(data["data"])>0):
        db.tododb.insert_one(data)
    #---------------------------------------------------------------------------------------------------
    data_saved = ""
    return flask.render_template('calc.html')

#---------------------------------------------------------------------------------------------------
@app.route('/_display',methods=['POST'])
def display():
    #---------------------------------------------------------------------------------------------------
    items = [{"data":""}]

    #---------------------------------------------------------------------------------------------------
    if db.tododb.count() == 0:
        items = [{"data":""}]
    else:
        _items = db.tododb.find()
        items = [item for item in _items]
        #---------------------------------------------------------------------------------------------------
        for item in items:
            del item['_id']
    #---------------------------------------------------------------------------------------------------
    return render_template('todo.html', items=items)



#---------------------------------------------------------------------------------------------------
def get_all_list(items):
    res_list = []
    res = [item.split() for item in items]

    lens = len(res)
    #---------------------------------------------------------------------------------------------------
    for i in range(lens):
        for j in range(len(res[i])):
            res[i][j] = res[i][j].replace('<br/>','')

    #---------------------------------------------------------------------------------------------------
    for i in range(len(res)):
        for j in range(len(res[i])):
            if res[i][j] == "Open:" or res[i][j] == "Close:":
                 res_list.append(res[i][j]+res[i][j+1])
                 j+=1
    return res_list

#---------------------------------------------------------------------------------------------------
def get_open_list(all_list):
    open_list = []
    for item in all_list:
        if "Open:" in item:
            open_list.append(item)
    return open_list

#---------------------------------------------------------------------------------------------------
def get_close_list(all_list):
    close_list = []
    for item in all_list:
        if "Close:" in item:
            close_list.append(item)
    return close_list

#---------------------------------------------------------------------------------------------------
def get_all_lsit_csv(all_list):
    csv_list = []
    for i in range(len(all_list)):
        temp = all_list[i]
        if "Open:" in temp or "Close:" in temp:
            temp = temp.split(":")
            csv_list.append(temp[0]+", "+temp[1]+":"+temp[2]+":"+temp[3]+":"+temp[4]+"<br/>")
    return csv_list

#---------------------------------------------------------------------------------------------------
def get_all_list_json(all_list):
    json_list = []

    for i in range(len(all_list)):
        temp = all_list[i]
        if "Open:" in temp:
            temp = temp.split(":")
            temp2 = all_list[i+1].split(":")
            #---------------------------------------------------------------------------------------------------
            json_list.append({
                temp[0]:temp[1]+":"+temp[2]+":"+temp[3]+":"+temp[4],
                temp2[0]:temp2[1]+":"+temp2[2]+":"+temp2[3]+":"+temp2[4]
                })
            i +=1
    return json_list

#---------------------------------------------------------------------------------------------------
def get_open_close_list_json(lst):
    json_list = []

    for i in range(len(lst)):
        temp = lst[i]
        if "Open:" in temp or "Close:" in temp:
            temp = temp.split(":")
            json_list.append({
                temp[0]:temp[1]+":"+temp[2]+":"+temp[3]+":"+temp[4]
                })
    return json_list


#---------------------------------------------------------------------------------------------------
def get_open_time(open_time_json):
    return (open_time_json["Open"])
def get_close_time(close_time_json):
    return (close_time_json["Close"])

#----------------------------------------------------------------------------------------------------
class ListAll(Resource):
    def get(self):

        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        return all_list


class ListAllOpenOnly(Resource):
    def get(self):

        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        open_list = get_open_list(all_list)
        return open_list


class ListAllCloseOnly(Resource):
    def get(self):

        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        close_list = get_close_list(all_list)

        return close_list




class ListAllFormatted(Resource):
    def get(self,formatted):
        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        if formatted == "csv":
            csv_string = get_all_lsit_csv(all_list)
            return csv_string
        elif formatted == "json":
            json_list = get_all_list_json(all_list)
            return json_list


class ListOpenOnlyFormatted(Resource):
    def get(self,formatted):
        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        open_list = get_open_list(all_list)
        top = request.args.get("top", type = str)
        if formatted == "csv":
            open_csv = []
            #Check for top is null or not
            if top:
                open_json = get_open_close_list_json(open_list)
                open_json.sort(key = get_open_time)
                res = open_json[:int(top)]
                for it in res:
                    open_csv.append("Open, "+it["Open"] +"<br/>")
                return open_csv
            else:
                open_csv = get_all_lsit_csv(open_list)
                return open_csv
        elif formatted == "json":
            open_json = get_open_close_list_json(open_list)
            if top:
                open_json.sort(key = get_open_time)
                res = open_json[:int(top)]
                return res
            return open_json


class ListCloseOnlyFormatted(Resource):
    def get(self, formatted):
        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]

        all_list = get_all_list(items)
        close_list = get_close_list(all_list)
        top = request.args.get("top", type = str)
        if formatted == "csv":
            close_csv = []
            
            if top:
                close_json = get_open_close_list_json(close_list)
                close_json.sort(key = get_close_time)
                res = close_json[:int(top)]
                for it in res:
                    close_csv.append("Close, "+it["Close"]+"<br/>")
                return close_csv
            else:
                close_csv = get_all_lsit_csv(close_list)
                return close_csv
        elif formatted == "json":
            close_json = get_open_close_list_json(close_list)
            if top:
                close_json.sort(key = get_close_time)
                res = close_json[:int(top)]
                return res
            return close_json




api.add_resource(ListAll,'/listAll')
api.add_resource(ListAllOpenOnly,'/listOpenOnly')
api.add_resource(ListAllCloseOnly,'/listCloseOnly')
api.add_resource(ListAllFormatted,'/listAll/<formatted>')
api.add_resource(ListOpenOnlyFormatted,'/listOpenOnly/<formatted>')
api.add_resource(ListCloseOnlyFormatted,'/listCloseOnly/<formatted>')



app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
